<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/upload', 'UploadController@index');
Route::post('/upload', 'UploadController@upload');
Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::middleware(['auth'])->prefix('posts')->group(function() {
    Route::get('/', 'PostController@index');
    Route::post('new', 'PostController@store')->name('post.new');
    Route::get('new', 'PostController@new')->name('post.new');
    Route::get('/{id}/show', 'PostController@show')->name('post.show');
    Route::get('/{id}/edit', 'PostController@edit')->name('post.edit');
});
