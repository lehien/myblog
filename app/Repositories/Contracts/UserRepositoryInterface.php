<?php
namespace App\Repositories\Contracts;

interface UserRepositoryInterface {

    public function all();

    public function find($id);

    public function findBy($col, $val);

    public function paginate($param);
    ]
}