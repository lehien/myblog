<?php
namespace App\Repositories\Contracts;

interface PostRepositoryInterface {

    public function all();

    public function find($id);

    public function findBy($col, $val);

    public function paginate($param);
    
    public function create($input);
}