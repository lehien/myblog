<?php 
namespace App\Repositories;

use App\Repositories\Contracts\PostRepositoryInterface;
use App\Http\Models\Post;

class PostRepository implements PostRepositoryInterface 
{
    
    protected $model;

    public function __construct(Post $model)
    {
        $this->model = $model;
    }

    public function all() {
        return $this->model->all();
    }

    public function find($id) {
        return $this->model->find($id);
    }

    public function findBy($col, $val)
    {
        return $this->model->where($col, $val)->get();
    }

    public function paginate($param = 10)
    {
        return $this->model->paginate($param);
    }

    public function create($input)
    {
        return $this->model->create($input);
    }
}
