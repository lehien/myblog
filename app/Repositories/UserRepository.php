<?php 
namespace App\Repositories;

use App\Repositories\Contracts\UserRepositoryInterface;
use App\Http\Models\User;

class UserRepository implements UserRepositoryInterface 
{
    
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function all() {
        return $this->user->all();
    }

    public function find($id) {
        return $this->user->find($id);
    }

    public function findBy($col, $val)
    {
        return $this->user->where($col, $val)->get();
    }

}