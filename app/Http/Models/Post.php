<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Models\User;

class Post extends Model
{
    protected $fillable = [
        'title', 'content', 'user_id', 'views', 'category', 'status'
    ];

    public function user() {
        $this->belongsTo(User::class, 'user_id');
    }


}
