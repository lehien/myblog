<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    const MODE_LIST = 'list';
    const MODE_SHOW = 'show';
    const MODE_EDIT = 'edit';
    const MODE_NEW = 'new';
    const MODE_UPLOAD = 'upload';
    
    const PAGES = [
        'index' => 'index',
        'new' => 'input',
        'edit' => 'input',
        'show' => 'input',
    ];

    protected $mode;
    protected $root_dir;
    protected $input_page;
    protected $list_page;
    protected $view;
    protected $data;

    public function __construct() {
        $this->data = array();
        $this->list_page = self::PAGES['index'];
        $this->input_page = self::PAGES['new'];
    }   

    public function setMode($mode) {
        $this->mode = $mode;
    }

    public function getMode() {
        return $this->mode;
    }

    public function setView($page) {
        $this->view = $page;
    }

    public function getView() {
        return $this->view;
    }

    public function setData($data) {
        foreach ($data as $key => $value) {
            $this->data[$key] = $value;
        }
    }

    public function getData() {
        return $this->data;
    }

    public function output() {
        return view($this->getView())
            ->with($this->getData());
    }

    public function index() {
        $this->setMode(self::MODE_LIST);
        $view_path = $this->root_dir.'.'.$this->list_page;
        $this->setView($view_path);
        return $this->output();
    }

    public function new() {
        $this->setMode(Controller::MODE_NEW);
        $view_path = $this->root_dir.'.'.$this->input_page;
        $this->setView($view_path);
        return $this->output();
    }

    public function edit($id) {
        $this->setMode(Controller::MODE_EDIT);
        $view_path = $this->root_dir.'.'.$this->input_page;
        $this->setView($view_path);
        return $this->output();
    }

    public function show($id) {
        $this->setMode(Controller::MODE_SHOW);
        $view_path = $this->root_dir.'.'.$this->input_page;
        $this->setView($view_path);
        return true;
    }
}
