<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Contracts\PostRepositoryInterface;
use App\Http\Models\Post;

class PostController extends Controller
{
    protected $post;
    protected $redirect; //default redirect

    public function __construct(PostRepositoryInterface $post) {
        $this->post = $post;
        $this->redirect = '/posts';
        $this->root_dir = 'posts';
        $this->list_page = self::PAGES['index'];
        $this->input_page = self::PAGES['new'];
    }

    public function index() {
        $this->setMode(self::MODE_LIST);
        $view_path = $this->root_dir.'.'.$this->list_page;
        $this->setView($view_path);
        $this->setData(['posts' => $this->post->paginate(10)]);
        
        return $this->output();
    }

    // public function find($id) {
    //     return $this->post;
    // }

    public function store(Request $request) {
        $input = $request->all();
        $input['user_id'] = Auth()->user()->id;
        $this->post->create($input);
        return redirect($this->redirect);
    }

    public function edit($id) {
        $this->setMode(self::MODE_SHOW);
        return view('posts.form_post_input')
            ->with('post', $this->post->find($id));
    }

    public function show($id) {
        if (parent::show($id)) {
            $this->setData(['post' => $this->post->find($id)]);
            return $this->output();
        } else {
            $this->index();
        }
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'title' => 'required|string|max:255',
            'content' => 'required|string',
            'user_id' => 'required|number'
        ]);
    }
}
