<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Contracts\UserRepositoryInterface as User;

class UserController extends Controller
{

    protected $user;

    public function __contruct(User $user) {
        $this->user = $user;
    }

    public function index() {
        return $this->user->all();
    }

    public function find($id) {

    }

    public function create($input) {

    }
}
