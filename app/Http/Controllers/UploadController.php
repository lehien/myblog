<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    public function index() {
        return view('upload.form_upload');
    }

    public function upload(Request $request) {
        $file = $request->file('fileToUpload');
        Storage::disk('ftp')->put('/admin', $file);
        return "success";
    }
}
