@extends('layouts.app') 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <a class="btn btn-small btn-primary" href="{{ route('post.new') }}">Create new</a>
                <div class="panel-heading">Post list</div>
            </div>
            @foreach ($posts as $post)
                <div class="panel-body">
                <a href="{{ route('post.show', ['id' => $post->id]) }}">{{$post->title}}</a>
                <span>{{$post->created_at}}</span>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection